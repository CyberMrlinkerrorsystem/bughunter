# mrlinkerrorsystem developer gitlab :)
# Introduction

This is a template for doing Android development using GitLab and [fastlane](https://fastlane.tools/).
It is based on the tutorial for Android apps in general that can be found [here](https://developer.android.com/training/basics/firstapp/). 
If you're learning Android at the same time, you can also follow along that
tutorial and learn how to do everything all at once.

# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [Blog post: Android publishing with GitLab and fastlane](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/)

You'll definitely want to read through the blog post since that walks you in detail
through a working production configuration using this model.

# Getting started

First thing is to follow the [Android tutorial](https://developer.android.com/training/basics/firstapp/) and
get Android Studio installed on your machine, so you can do development using
the Android IDE. Other IDE options are possible, but not directly described or
supported here. If you're using your own IDE, it should be fairly straightforward
to convert these instructions to use with your preferred toolchain.

## What's contained in this project

### Android code

The state of this project is as if you followed the first few steps in the linked
[Android tutorial](https://developer.android.com/training/basics/firstapp/) and
have created your project. You're definitely going to want to open up the
project and change the settings to match what you plan to build. In particular,
you're at least going to want to change the following:

- Application Name: "My First App"
- Company Domain: "example.com"

### Fastlane files

It also has fastlane setup per our [blog post](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/) on
getting GitLab CI set up with fastlane. Note that you may want to update your
fastlane bundle to the latest version; if a newer version is available, the pipeline
job output will tell you.

### Dockerfile build environment

In the root there is a Dockerfile which defines a build environment which will be
used to ensure consistent and reliable builds of your Android application using
the correct Android SDK and other details you expect. Feel free to add any
build-time tools or whatever else you need here.

We generate this environment as needed because installing the Android SDK
for every pipeline run would be very slow.

### Gradle configuration

The gradle configuration is exactly as output by Android Studio except for the
version name being updated to 

Instead of:

`versionName "1.0"`

It is now set to:

`versionName "1.0-${System.env.VERSION_SHA}"`

You'll want to update this for whatever versioning scheme you prefer.

### Build configuration (`.gitlab-ci.yml`)

The sample project also contains a basic `.gitlab-ci.yml` which will successfully 
build the Android application.

Note that for publishing to the test channels or production, you'll need to set
up your secret API key. The stub code is here for that, but please see our
[blog post](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/) for
details on how to set this up completely. In the meantime, publishing steps will fail.

The build script also handles automatic versioning by relying on the CI pipeline
ID to generate a unique, ever increasing number. If you have a different versioning
scheme you may want to change this.

```yaml
    - "export VERSION_CODE=$(($CI_PIPELINE_IID)) && echo $VERSION_CODE"
    - "export VERSION_SHA=`echo ${CI_COMMIT_SHA:0:8}` && echo $VERSION_SHA"
``
<p align="center"><img src="https://gitlab.com/cybermlinkerrorsystem/BugHunter/

## Bug Hunter Menu :

- Information Gathering
- Mapping
- Discovery 
- Exploitation
- PoCs & Reporting

### Information Gathering :

- Basic Commands for Information Gathering
- Masscan - TCP Port Scanner
- DNS Recon - DNS Enumeration
- Sublist3r - Find Subdomains
- Alt-DNS - Subdomain Discovery
- Amass - In-Depth DNS Enumeration
- Subfinder - Subdomain Discovery Tool
- Enumall - Setup Script for Regon-NG
- Aquatone - Reconnaissance on Domain Names
- Cloudflare_Enum - Cloudflare DNS Enumeration
- InfoG - Information Gathering Tool
- The Harvester - E-mail, SubDomain, Ports etc.
- Recon-NG - Web Reconnaissance Framework
- SetoolKit - Social Engineering Toolkit
- WhatWeb - Next Generation Web Scanner
- Maltego - Interactive Data Mining Tool    
    
### Mapping :     
    
- Nmap - IP's, Open Ports and Much More
- Firefox - Web Browser
- Firefox Browser Extensions
- Burp Suite Pro
- Burp Suite Extensions
- Intruder Payloads for Burp Suite
- Payloads All The Thing
    
### Discovery :
    
- Acunetix-WVS
- Arachni
- Burp Suite
- Nexpose
- Nikto
- Vega
- Wapiti
- Web Security Scanner
- Websecurify Suite
- Joomscan
- w3af
- Zed Attack Proxy
- WP-Scan
- FuzzDB
- CeWL

### Exploitation :

XSS :
- XSS Radar
- XSSHunter
- xssHunter Client
- DOMxssScanner
- XSSer
- BruteXSS
- XSStrike
- XSS'OR
            
SQLi :  
- SQLmap

XXE : 
- OXML-xxe
- XXEinjextor

SSTI :
- Tplmap

SSRF :
- SSRF-Detector
- Ground Control

LFI :
- LFISuit

Mobile :
- MobSF
- GenyMotion
- Apktool
- dex2jar
- jd-gui
- idb

Other : 
- Gen-xbin-Avi
- GitTools
- DVCS Ripper
- TKO Subs
- SubBruteforcer
- Second-Order
- Race The Web
- CORStest
- RCE Struts-pwn
- ysoSerial
- PHPGGC
- Retire-js
- Getsploit
- Findsploit
- BFAC
- WP-Scan
- CMSmap
- Joomscan
- JSON W T T
- Wfuzz
- Patator
- Netcat
- ChangeMe
- wappalyzer
- builtwith
- wafw00f
- assetnote
- jsbeautifier
- LinkFinder

### PoCs & Reporting :

- Bug Bounty Platforms
- POCs (Proof of Concepts)
- CheatSheet
- EyeWitness
- HttpScreenshot
- BugBountyTemplates
- Template Generator

## How To Install :

```git clone https://gitlab.com/CyberMrlinkerrorsystem/bughunter.git && cd bughunter && chmod +x bughunter.py && sudo cp bughunter.py /usr/bin/bughunter```

that's it.. type ***bughunter*** in terminal to execute the tool.

## Download Directory :
   
Normal User : /home/$USER/bughunter/

Root User : /root/bughunter/

- ~/bughunter/info/ : Tools for Information Gathering
- ~/bughunter/mapp/ : Tools for Mapping
- ~/bughunter/disc/ : Tools for Discovery
- ~/bughunter/expt/ : Tools for Exploitation
- ~/bughunter/rept/ : Tools for Reporting
- ~/bughunter/sage/ : Tools by Mr. Team

View Tool's README.md File for Installation Instruction and How To Use Guide.

## Source : 

TBHM3, GitLab, Bug Bounty Forum, Google and Few Bug Hunting Articles.

## License :

[MIT Licence](https://gitlab.com/CyberMrlinkerrorsystem/bughunter/blob/master/LICENSE)

That's it... If You Like This Repo. Please Share This With Your Friends..

& Don't Forget To Follow Me At [Twitter](https://www.twitter.com/Sontoloyo), [Instagram](https://www.instagram.com/Cyber_Mrlink_Error_System_2019), [Github](https://www.github.com/404System) 


***Thankyou.***
***Happy Hunting..***
